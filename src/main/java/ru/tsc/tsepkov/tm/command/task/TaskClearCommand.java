package ru.tsc.tsepkov.tm.command.task;

import org.jetbrains.annotations.NotNull;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-clear";

    @NotNull
    public static final String DESCRIPTION = "Remove all tasks.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        @NotNull final String userId = getUserId();
        getTaskService().removeAll(userId);
    }

}
