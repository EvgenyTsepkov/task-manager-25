package ru.tsc.tsepkov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

public class AbstractUserOwnedModel extends AbstractModel {

    @Getter
    @Setter
    @Nullable
    private String userId;

}
