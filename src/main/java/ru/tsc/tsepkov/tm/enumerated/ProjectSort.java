package ru.tsc.tsepkov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tsepkov.tm.comparator.CreatedComparator;
import ru.tsc.tsepkov.tm.comparator.NameComparator;
import ru.tsc.tsepkov.tm.comparator.StatusComparator;
import ru.tsc.tsepkov.tm.model.Project;

import java.util.Comparator;

public enum ProjectSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_DEFAULT("Sort by default", null);

    @Getter
    @NotNull
    private final String name;

    @Getter
    @Nullable
    private final Comparator<Project> comparator;

    @NotNull
    public static ProjectSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return BY_DEFAULT;
        for (@NotNull final ProjectSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return BY_DEFAULT;
    }

    ProjectSort(@NotNull final String name, @Nullable Comparator<Project> comparator) {
        this.name = name;
        this.comparator = comparator;
    }

}
