package ru.tsc.tsepkov.tm.exception.field;

public final class UserIdEmptyException extends  AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! UserId is empty...");
    }

}
