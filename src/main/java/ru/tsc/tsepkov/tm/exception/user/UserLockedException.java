package ru.tsc.tsepkov.tm.exception.user;

public final class UserLockedException extends AbstractUserException {

    public UserLockedException() {
        super("Error! You are not logged in. This user is locked, please contact the administrator...");
    }

}
