package ru.tsc.tsepkov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

}
